package pl.pjatk.bmicalculator

import android.os.Bundle
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import pl.pjatk.bmicalculator.models.QuizModel

class QuizActivity : AppCompatActivity() {
    private var mTxtQuestion: TextView? = null
    private var questionIndex = 0
    private var quizQuestion = 0
    private var progressBar: ProgressBar? = null
    private var statsTextView: TextView? = null
    private var userScore = 0
    private val questionCollection = arrayOf(
        QuizModel(R.string.quiz_question_1, false),
        QuizModel(R.string.quiz_question_2, true),
        QuizModel(R.string.quiz_question_3, false),
        QuizModel(R.string.quiz_question_4, true),
        QuizModel(R.string.quiz_question_5, true),
        QuizModel(R.string.quiz_question_6, false)
    )
    val USER_PROGRESS = Math.ceil(100.0 / questionCollection.size).toInt()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        var mTxtQuestion = findViewById<TextView>(R.id.txtQuestion)
        val q1 = questionCollection[questionIndex]
        quizQuestion = q1.question
        mTxtQuestion.setText(quizQuestion)
        progressBar = findViewById(R.id.quizPB)
        var statsTextView = findViewById<TextView>(R.id.txtQuizStats)
        statsTextView.text = "$userScore/ 6"
        val buttonTrue = findViewById<Button>(R.id.btnTrue)
        buttonTrue.setOnClickListener {
            evaluateUserAnswer(true)
            changeQuestionOnButtonClick()
        }
        val buttonFalse = findViewById<Button>(R.id.btnFalse)
        buttonFalse.setOnClickListener {
            evaluateUserAnswer(false)
            changeQuestionOnButtonClick()
        }
    }

    private fun changeQuestionOnButtonClick() {
        questionIndex = (questionIndex + 1) % 6
        statsTextView!!.text = "$userScore/ 6"
        if (questionIndex == 0) {
            val quizAlert = AlertDialog.Builder(this)
            quizAlert.setCancelable(false)
            quizAlert.setTitle("Quiz is Finished")
            quizAlert.setMessage("Your Score is $userScore")
            quizAlert.setPositiveButton("Finish the Quiz") { dialogInterface, i -> finish() }
            quizAlert.show()
        }
        quizQuestion = questionCollection[questionIndex].question
        mTxtQuestion!!.setText(quizQuestion)
        progressBar!!.incrementProgressBy(USER_PROGRESS)
    }

    private fun evaluateUserAnswer(userGuess: Boolean) {
        val currentQuestionAnswer = questionCollection[questionIndex].isAnswer
        if (currentQuestionAnswer == userGuess) {
            userScore = userScore + 1
        }
    }
}