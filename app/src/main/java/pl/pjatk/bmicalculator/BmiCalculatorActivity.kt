package pl.pjatk.bmicalculator

import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.NumberFormat

class BmiCalculatorActivity : AppCompatActivity() {
    private var weight = 50.0
    private var weightTextView: TextView? = null
    private var height = 160.0
    private var heightTextView: TextView? = null
    private val bmi = 0.0
    private var bmiTextView: TextView? = null

    // called when the activity is first created
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // call superclass onCreate
        setContentView(R.layout.activity_bmi) // inflate the GUI

        // get references to programmatically manipulated TextViews
        heightTextView = findViewById<View>(R.id.heightTextView) as TextView
        weightTextView = findViewById<View>(R.id.weightTextView) as TextView
        bmiTextView = findViewById<View>(R.id.bmiTextView) as TextView
        bmiTextView!!.text = bmiFormat.format(0)

        // set weightSeekBar's OnSeekBarChangeListener
        val weightSeekBar = findViewById<View>(R.id.weightSeekBar) as SeekBar
        weightSeekBar.setOnSeekBarChangeListener(weightSeekBarListener)
        val heightSeekBar = findViewById<View>(R.id.heightSeekBar) as SeekBar
        heightSeekBar.setOnSeekBarChangeListener(heightSeekBarListener)
    }

    // calculate and display tip and total amounts
    private fun calculate() {
        val bmi = weight / Math.pow(height, 2.0)
        bmiTextView!!.text = bmiFormat.format(bmi)
    }

    // listener object for the SeekBar's progress changed events
    private val weightSeekBarListener: OnSeekBarChangeListener = object : OnSeekBarChangeListener {
        // update percent, then call calculate
        override fun onProgressChanged(
            weightSeekBar: SeekBar, progress: Int,
            fromUser: Boolean
        ) {
            weight = weightSeekBar.progress.toDouble()
            weightTextView!!.text = weight.toString() + "kg"
            calculate() // calculate and display tip and total
        }

        override fun onStartTrackingTouch(weightSeekBar: SeekBar) {}
        override fun onStopTrackingTouch(weightSeekBar: SeekBar) {}
    }
    private val heightSeekBarListener: OnSeekBarChangeListener = object : OnSeekBarChangeListener {
        // update percent, then call calculate
        override fun onProgressChanged(
            heightSeekBar: SeekBar, progress: Int,
            fromUser: Boolean
        ) {
            height = (heightSeekBar.progress / 100.0)
            heightTextView!!.text = height.toString() + "m"
            calculate() // calculate and display tip and total
        }

        override fun onStartTrackingTouch(heightSeekBar: SeekBar) {}
        override fun onStopTrackingTouch(heightSeekBar: SeekBar) {}
    }

    companion object {
        //    // currency and percent formatter objects
        private val bmiFormat = NumberFormat.getNumberInstance()
    }
}