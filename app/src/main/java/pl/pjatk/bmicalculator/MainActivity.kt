package pl.pjatk.bmicalculator

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import pl.pjatk.bmicalculator.BmiCalculatorActivity

class MainActivity : AppCompatActivity() {
    private var BMIButton: Button? = null
    private var CaloriesButton: Button? = null
    private var RecipesButton: Button? = null
    private var QuizButton: Button? = null
    private var ChartButton: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // call superclass onCreate
        setContentView(R.layout.activity_main) // inflate the GUI
        var BMIButton = findViewById<Button>(R.id.buttonToBmiActivity)
        var CaloriesButton = findViewById<Button>(R.id.buttonToCaloriesActivity)
        var RecipesButton = findViewById<Button>(R.id.buttonToRecipesActivity)
        var QuizButton = findViewById<Button>(R.id.buttonToQuizActivity)
        var ChartButton = findViewById<Button>(R.id.buttonToChartActivity)
        BMIButton.setOnClickListener(View.OnClickListener { openBmiActivity() })
        CaloriesButton.setOnClickListener(View.OnClickListener { openCaloriesActivity() })
        RecipesButton.setOnClickListener(View.OnClickListener { openRecipesActivity() })
        QuizButton.setOnClickListener(View.OnClickListener { openQuizActivity() })
        ChartButton.setOnClickListener(View.OnClickListener { openChartActivity() })
    }

    private fun openRecipesActivity() {
        val intent = Intent(this, RecipesActivity::class.java)
        startActivity(intent)
    }

    private fun openCaloriesActivity() {
        val intent = Intent(this, CaloriesActivity::class.java)
        startActivity(intent)
    }

    private fun openBmiActivity() {
        val intent = Intent(this, BmiCalculatorActivity::class.java)
        startActivity(intent)
    }

    private fun openQuizActivity() {
        val intent = Intent(this, QuizActivity::class.java)
        startActivity(intent)
    }

    private fun openChartActivity() {
        val intent = Intent(this, ChartActivity::class.java)
        startActivity(intent)
    }
}